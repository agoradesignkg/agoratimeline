(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.initVerticalTimeline = {
    attach: function (context) {
      let timelines = once('timeline', '.timeline', context);
      timelines.forEach(function (elem) {
        $(elem).verticalTimeline({
          startSide: 'right',
          alternate: true,
          animate: 'fade'
        });
      });
    }
  };
})(jQuery, Drupal);
