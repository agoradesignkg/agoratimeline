<?php

namespace Drupal\agoratimeline\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Defines the interface for timeline entry entities.
 */
interface TimelineEntryInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the year (milestone label).
   *
   * @return string
   *   The year (milestone label).
   */
  public function getYear();

  /**
   * Sets the year (milestone label).
   *
   * @param string $year
   *   The year (milestone label).
   *
   * @return $this
   */
  public function setYear($year);

  /**
   * Gets the milestone title.
   *
   * @return string
   *   The milestone title
   */
  public function getTitle();

  /**
   * Sets the milestone title.
   *
   * @param string $title
   *   The milestone title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Gets the milestone text.
   *
   * @return string
   *   The milestone text.
   */
  public function getText();

  /**
   * Sets the milestone text.
   *
   * @param string $text
   *   The milestone text.
   *
   * @return $this
   */
  public function setText($text);

  /**
   * Gets the creation timestamp.
   *
   * @return int
   *   The creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

}
