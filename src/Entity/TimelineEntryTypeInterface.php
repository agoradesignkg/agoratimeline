<?php

namespace Drupal\agoratimeline\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for timeline entry types.
 */
interface TimelineEntryTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
