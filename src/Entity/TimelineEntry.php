<?php

namespace Drupal\agoratimeline\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the timeline entry entity class.
 *
 * @ContentEntityType(
 *   id = "timeline_entry",
 *   label = @Translation("Timeline entry"),
 *   label_singular = @Translation("timeline entry"),
 *   label_plural = @Translation("timeline entries"),
 *   label_count = @PluralTranslation(
 *     singular = "@count timeline entry",
 *     plural = "@count timeline entries",
 *   ),
 *   bundle_label = @Translation("Timeline entry type"),
 *   handlers = {
 *     "access" = "Drupal\agoratimeline\TimelineEntryAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "add" = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler"
 *   },
 *   admin_permission = "administer agoratimeline",
 *   base_table = "timeline_entry",
 *   data_table = "timeline_entry_data",
 *   revision_table = "timeline_entry_revision",
 *   revision_data_table = "timeline_entry_data_revision",
 *   translatable = TRUE,
 *   content_translation_ui_skip = TRUE,
 *   show_revision_ui = TRUE,
 *   entity_keys = {
 *     "id" = "timeline_entry_id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "year",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   links = {
 *     "add-page" = "/timeline-entry/add",
 *     "add-form" = "/timeline-entry/add/{timeline_entry_type}",
 *     "edit-form" = "/timeline-entry/{timeline_entry}/edit",
 *     "delete-form" = "/timeline-entry/{timeline_entry}/delete",
 *   },
 *   bundle_entity_type = "timeline_entry_type",
 *   field_ui_base_route = "entity.timeline_entry_type.edit_form",
 * )
 */
class TimelineEntry extends ContentEntityBase implements TimelineEntryInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getYear() {
    return $this->get('year')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setYear($year) {
    $this->set('year', $year);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getText() {
    return $this->get('text')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setText($text) {
    $this->set('text', $text);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['year'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Year'))
      ->setDescription(t('The timeline entry year or milestone label.'))
      ->setDefaultValue('')
      ->setRequired(FALSE)
      ->setTranslatable(TRUE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 32)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The timeline entry title.'))
      ->setRequired(FALSE)
      ->setTranslatable(TRUE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Text'))
      ->setDescription(t('The timeline entry text.'))
      ->setRequired(FALSE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the timeline entry was created.'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the timeline entry was last edited.'));

    return $fields;
  }

}
