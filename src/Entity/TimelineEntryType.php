<?php

namespace Drupal\agoratimeline\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the timeline entry type entity class.
 *
 * @ConfigEntityType(
 *   id = "timeline_entry_type",
 *   label = @Translation("Timeline entry type"),
 *   label_singular = @Translation("timeline entry type"),
 *   label_plural = @Translation("timeline entry types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count timeline entry type",
 *     plural = "@count timeline entry types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\agoratimeline\Form\TimelineEntryTypeForm",
 *       "edit" = "Drupal\agoratimeline\Form\TimelineEntryTypeForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\agoratimeline\TimelineEntryTypeListBuilder",
 *   },
 *   config_prefix = "timeline_entry_type",
 *   admin_permission = "administer agoratimeline",
 *   bundle_of = "timeline_entry",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/timeline-entry-types/add",
 *     "edit-form" = "/admin/structure/timeline-entry-types/{timeline_entry_type}/edit",
 *     "collection" = "/admin/structure/timeline-entry-types"
 *   }
 * )
 */
class TimelineEntryType extends ConfigEntityBundleBase implements TimelineEntryTypeInterface {

  /**
   * The timeline entry type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The timeline entry type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The timeline entry type description.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
