<?php

namespace Drupal\agoratimeline\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsEntityFormatter;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "timeline_item_aggregated_year",
 *   label = @Translation("Aggregated year"),
 *   description = @Translation("Display the timeline items aggregated by year."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class AggregatedYearTimelineItemFormatter extends EntityReferenceRevisionsEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items $view_mode */
    $view_mode = $this->getSetting('view_mode');
    $elements = [];
    /** @var \Drupal\agoratimeline\Entity\TimelineEntryInterface[] $entities_to_view */
    $entities_to_view = $this->getEntitiesToView($items, $langcode);

    $grouped_entities = [];
    foreach ($entities_to_view as $entity) {
      $year = $entity->getYear();
      if (empty($year)) {
        continue;
      }
      $grouped_entities[$year][] = $entity;
    }

    $grouped_items = [];
    foreach ($grouped_entities as $year => $entities) {
      $grouped_items[$year] = [];
      foreach ($entities as $delta => $entity) {
        $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity->getEntityTypeId());
        $grouped_items[$year][$delta] = $view_builder->view($entity, $view_mode, $entity->language()->getId());
      }
    }
    $elements[] = [
      '#theme' => 'timeline_aggregated_year',
      '#items' => $grouped_items,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $is_applicable = parent::isApplicable($field_definition);

    if ($is_applicable) {
      $target_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
      $is_applicable = $target_type === 'timeline_entry';
    }

    return $is_applicable;
  }

}
