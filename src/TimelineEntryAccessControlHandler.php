<?php

namespace Drupal\agoratimeline;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the timeline entry entity.
 *
 * @see \Drupal\agoratimeline\Entity\TimelineEntry.
 */
class TimelineEntryAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\agoratimeline\Entity\TimelineEntryInterface $entity */
    if ($operation === 'view') {
      $access_result = AccessResult::allowedIf($entity->isPublished() || $account->hasPermission('view unpublished timeline_entry'));
    }
    else {
      $access_result = AccessResult::allowed();
    }
    return $access_result;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    // Allow paragraph entities to be created in the context of entity forms.
    if (\Drupal::request()->getRequestFormat() === 'html') {
      return AccessResult::allowed()->addCacheContexts(['request_format']);
    }
    return AccessResult::neutral()->addCacheContexts(['request_format']);
  }

}
